/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zuleima
 */
public class vuelo {
    private int totalsupervip;
    private int totalvip;
    private int totalturista;
    List<Lugar> puestos= new ArrayList<>();

    public vuelo(int totalsupervip, int totalvip, int totalturista) {
        this.totalsupervip = totalsupervip;
        this.totalvip = totalvip;
        this.totalturista = totalturista;
        for (int i = 0; i < totalsupervip; i++) {
            puestos.add(new Lugar("SVIP" + i, null, false));
            
        }
for (int i = 0; i < totalvip; i++) {
            puestos.add(new Lugar("VIP" + i, null, false));
            
        }        
for (int i = 0; i < totalturista; i++) {
            puestos.add(new Lugar("REG" + i, null, false));
            
        }        
        
    }
    
    
    
    
    
    public boolean asignarLugar(Cliente c, String tipo){
        if(tipo.equals("VIP")   ){
            for(  Lugar l : puestos){
               if( l instanceof VIP){
                   if( l.ocupado == false){
                       l.cli= c;
                       return true;
                   }
                   
               }
            }
           return false;
        }
        
        
        if(tipo.equals("SVIP")   ){
            for(  Lugar l : puestos){
               if( l instanceof SVIP){
                   if( l.ocupado == false){
                       l.cli= c;
                       return true;
                   }
                   
               }
            }
           return false;
        }
        
        if(tipo.equals("REG")   ){
            for(  Lugar l : puestos){
               if( l instanceof REG){
                   if( l.ocupado == false){
                       l.cli= c;
                       return true;
                   }
                   
               }
            }
           return false;
        }
        return false;
    }
    
    
    public int getTotalsupervip() {
        return totalsupervip;
    }

    public void setTotalsupervip(int totalsupervip) {
        this.totalsupervip = totalsupervip;
    }

    public int getTotalvip() {
        return totalvip;
    }

    public void setTotalvip(int totalvip) {
        this.totalvip = totalvip;
    }

    public int getTotalturista() {
        return totalturista;
    }

    public void setTotalturista(int totalturista) {
        this.totalturista = totalturista;
    }
    
    public void horasalida(){
        
    }
    
    public void horallegada(){
        
    }
    
    public void cantidadcupos(){
        
    }
    
}

class Lugar{
    String num;
    Cliente cli;
    boolean ocupado;

    public Lugar(String num, Cliente cli, boolean ocupado) {
        this.num = num;
        this.cli = cli;
        this.ocupado = ocupado;
    }
    
    
    
}

class VIP extends Lugar{

    public VIP(String num, Cliente cli, boolean ocupado) {
        super(num, cli, ocupado);
    }
    
}


class SVIP extends Lugar{

    public SVIP(String num, Cliente cli, boolean ocupado) {
        super(num, cli, ocupado);
    }
    
}


class REG extends Lugar{

    public REG(String num, Cliente cli, boolean ocupado) {
        super(num, cli, ocupado);
    }
    
}

