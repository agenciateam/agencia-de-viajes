/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

/**
 *
 * @author zuleima
 */
public class Cliente {
    
    
    private String Nombre;
    private String Apellido;
    private String Correo;

    public Cliente(String Nombre, String Apellido, String Correo) {
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Correo = Correo;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the Apellido
     */
    public String getApellido() {
        return Apellido;
    }

    /**
     * @param Apellido the Apellido to set
     */
    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    /**
     * @return the Correo
     */
    public String getCorreo() {
        return Correo;
    }

    /**
     * @param Correo the Correo to set
     */
    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }
    
    private void varioscupos(){
        
    }
    
    private void cancelacion(){
        
    }
    private void mediopago(){
        
    }
    
    
}
