/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

/**
 *
 * @author zuleima
 */
public class Empresarial extends Cliente {
    private String nombreempresa;
    private int nitempresa;

    public Empresarial(String nombreempresa, int nitempresa, String Nombre, String Apellido, String Correo) {
        super(Nombre, Apellido, Correo);
        this.nombreempresa = nombreempresa;
        this.nitempresa = nitempresa;
    }

    public String getNombreempresa() {
        return nombreempresa;
    }

    public void setNombreempresa(String nombreempresa) {
        this.nombreempresa = nombreempresa;
    }
   

    public int getNitempresa() {
        return nitempresa;
    }

    public void setNitempresa(int nitempresa) {
        this.nitempresa = nitempresa;
    }
    
    
     private int Variocupos(){
        return 0;
        
    }
    
}
